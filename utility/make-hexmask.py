#!/usr/bin/python3

import sys

#+###############################################
# args
#+###############################################

def get_args():
    
    # help
    argv = sys.argv[1:]
    argc = len(argv)
    if argc < 1:
        sys.exit("No args were provided.")
    if argv[0] == "-h" or argv[0] == "--help":
        print(
            "\'make-hexmask\' is a utility that can be used to create hexadecimal masks for the "
            "\'--cpu-bind=mask_cpu\'\noption for a more intuitive and unchanging affinity experience.\n\n"
            "    Usage: make-hexmask (-n|--ntasks) <task count> (-c|--cpus-per-task) <cores per task>\n"
            "                        (-m|--machine) <((c|cluster)|(b|booster))> (-d|--distribution) <(b|c)>\n"
            "                        [-b|--bitfield]\n\n"
            "    Options:\n"
            "        -n|--ntasks <task count>                    The number of tasks to be spawned.\n"
            "        -c|--cpus-per-task <cores per task>         The number of cores per task.\n"
            "        -m|--machine <((c|cluster)|(b|booster))>    Indicates the machine of interest.\n"
            "                                                    Currently only the cluster is supported.\n"
            "        -d|--distribution <((b|block)|(c|cyclic))>  \'block\' fills the current NUMA domain with tasks\n"
            "                                                    before moving to the next. \'cyclic\' tries to fill\n"
            "                                                    all NUMA domains simultaneously.\n"
            "        [-b|--bitfield]                             Prints only the binary representation.\n"
            "\n    Support: Nitin Malapally (nitin.malapally@gmail.com)\n\n"
            )
        sys.exit(0)
    
    # optional args first
    args = {}
    args["-b"] = False
    if argc % 2 != 0:
        for ii in range(argc):
            if argv[ii] == "-b" or argv[ii] == "--bitfield":
                args["-b"] = True
                argv.pop(ii)
                argc -= 1
                break;
    
    # loop pre-conditions
    if argc % 2 != 0:
        sys.exit("Number of args must be even. Check args.")
    
    # loop
    for ii in range(0, argc, 2):
        arg = argv[ii]
        if arg == "-n" or arg == "--ntasks":
            value = int(argv[ii + 1])
            if value <= 0:
                sys.exit("Number of tasks may not be zero-valued or negative.")
            args["-n"] = value
            
        elif arg == "-c" or arg == "--cpus-per-task":
            value = int(argv[ii + 1])
            if value <= 0 or value > 48:
                sys.exit("Number of cores per task may not be zero-valued, negative or >48.")
            args["-c"] = value
            
        elif arg == "-m" or arg == "--machine":
            value = argv[ii + 1]
            if value != "c" and value != "cluster" and value != "b" and value != "booster":
                sys.exit("Invalid option provided for machine. ")
            args["-m"] = value
            
        elif arg == "-d" or arg == "--distribution":
            value = argv[ii + 1]
            if value != "b" and value != "block" and value != "c" and value != "cyclic":
                sys.exit("Invalid option provided for distribution.")
            args["-d"] = value
            
        else:
            sys.exit("Unrecognized option provided.")
    
    # post-conditions
    task_count = args["-n"]
    cores_per_task = args["-c"]
    total_core_count = task_count * cores_per_task
    if total_core_count > 48:
        if total_core_count % 48 != 0:
            sys.exit("Illegal total core count.")
        if task_count % (total_core_count / 48) != 0:
            sys.exit("More cores were requested than are available in a node.")
    
    # done
    return args

#+###############################################
# main function
#+###############################################

def main():
    
    # get args
    args = get_args()
    
    # "unpack" args
    task_count = args["-n"]
    cores_per_task = args["-c"]
    distribution = args["-d"]
    is_cluster = args["-m"] == "c" or args["-m"] == "cluster"
    
    # block distribution is the same for both machines
    total_core_count = task_count * cores_per_task
    task_count_per_node = task_count if total_core_count <= 48 else int(task_count / (total_core_count / 48))
    masks = [0] * task_count_per_node
    if distribution == "b" or distribution == "block":
        for ii in range(task_count_per_node):
            task_start_index = ii * cores_per_task
            for jj in range(cores_per_task):
                masks[ii] += 2**(task_start_index + jj)
    else:
        numa_start_indices = [0, 24] if is_cluster else [0, 6, 12, 18, 24, 30, 36, 42]
        mode_count = 2 if is_cluster else 8
        for ii in range(task_count_per_node):
            for jj in range(cores_per_task):
                numa_index = (ii + jj) % mode_count
                masks[ii] += 2**numa_start_indices[numa_index]
                numa_start_indices[numa_index] += 1
    
    # output
    if args["-b"]:
        print("Here are the binary masks as 48-bit bitfields:")
        for ii, mask in enumerate(masks):
            bitfield = format(mask, '048b')
            if is_cluster:
                bitfield = bitfield[:24] + " " + bitfield[24:]
            else:
                bitfield = "*" + bitfield[:6] + " " + bitfield[6:12] + " *" + bitfield[12:18] + " " + bitfield[18:24] \
                    + " *" + bitfield[24:30] + " " + bitfield[30:36] + " *" + bitfield[36:42] + " " + bitfield[42:48]
            print("    Task %2s: %s.\n" % (str(ii), bitfield))
    else:
        print("--ntasks=%u --cpu-bind=mask_cpu:" % (task_count), end="")
        for ii, mask in enumerate(masks):
            print_end = "," if ii + 1 < task_count_per_node else ""
            print(hex(mask), end=print_end)
        print(" ")


if __name__ == "__main__":
    main()
