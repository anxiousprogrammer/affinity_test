//+////////////////////////////////////////////////////////////////////////////////////////////////
//    affinity_test: a program to manually, visually test if a given affinity scheme works on
//    linux-based systems.
//    Copyright (C) 2024-2029 Nitin Malapally
//
//    This file is part of affinity_test.
//
//    affinity_test is free software: you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public License
//    as published by the Free Software Foundation, either version 3 of
//    the License, or (at your option) any later version.
//
//    affinity_test is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//    author: Nitin Malapally
//    email: nitin.malapally@gmail.com
//+////////////////////////////////////////////////////////////////////////////////////////////////

#include <cstdio> // std::printf
#include <string> // std::string
#include <vector> // std::vector

#include <sched.h> // sched_getcpu

#include <omp.h>

#include <mpi.h>

int main(int argc, char** argv)
{
    //+/////////////////
    // MPI stuff
    //+/////////////////
    
    auto provided = int{};
    MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &provided);
    if (provided < MPI_THREAD_FUNNELED)
    {
        std::fprintf(stderr, "Fatal error: Multi-threading support not available with the current MPI implementation.\n");
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
    }
    
    // rank
    auto world_rank = -1;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    if (world_rank < 0)
    {
        std::fprintf(stderr, "Fatal error: unable to obtain the rank in the communicator.\n");
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
    }
    
    // size
    auto world_size = -1;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    if (world_size < 0)
    {
        std::fprintf(stderr, "Fatal error: unable to obtain the size of the communicator.\n");
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
    }
    
    //+/////////////////
    // collect info
    //+/////////////////
    
    // create output on this rank
    const auto thread_count = omp_get_max_threads();
    auto output = std::vector(thread_count, -1);
    #pragma omp parallel for schedule(static, 1) ordered
    for (auto thread_index = int{}; thread_index < thread_count; ++thread_index)
    {
        #pragma omp ordered
        {
            output[omp_get_thread_num()] = sched_getcpu();
        }
    }
    
    // gather output on root
    auto gathered_output = std::vector(world_size * thread_count, -1);
    MPI_Gather(output.data(), output.size(), MPI_INT, gathered_output.data(), output.size(), MPI_INT, 0, MPI_COMM_WORLD);
    
    //+/////////////////
    // report
    //+/////////////////
    
    // root outputs in an orderly fashion
    if (world_rank == 0)
    {
        for (auto ii = 0u; ii < static_cast<unsigned>(world_size); ++ii)
        {
            for (auto jj = 0u; jj < static_cast<unsigned>(thread_count); ++jj)
            {
                if (gathered_output[ii * thread_count + jj] == -1)
                {
                    std::fprintf(stderr, "Fatal error: something went wrong while populating the thread to core mapping.\n");
                    MPI_Abort(MPI_COMM_WORLD, MPI_ERR_UNKNOWN);
                }
                std::printf("[Rank%5d] OMP thread #%3d found on core #%3d.\n", ii, jj, gathered_output[ii * thread_count + jj]);
            }
            std::printf("\n");
        }
    }
    
    // clean-up
    MPI_Finalize();
    
    return 0;
}
